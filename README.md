# Rendu TP Fedora

## I. systemd-basics

### 1. First steps :
- *s'assurer que systemd est PID1 :*

    `pgrep systemd`

- *check tous les processus excepté les process kernel :*

    `ps --ppid 2 -p 2 --deselect`

### 2. Gestion du temps
- *déterminer la différence entre Local Time, Universal Time et RTC time*
    - local time : heure de la time zone
    - utc : universal time clock (heure UTC)
    - rtc : real time clock

- *expliquer dans quels cas il peut être pertinent d'utiliser le RTC time.*

ça peut être pratique quand on utilise un soft ou outil avec une mesure ou utilisation très précise du temps.

- *changer de timezone pour un autre fuseau horaire européen*

`timedatectl set-timezone Europe/Rome`

- *désactiver le service lié à la synchronisation du temps avec cette commande, et vérifier à la main qu'il a été coupé*

### 3. Gestion des noms

- *expliquer la différence entre les trois types de noms. Lequel est à utiliser pour des machines de prod ?*

Le hostname static est celui renseigné dans le fichier /etc/hostname.
Le pretty sert juste pour l'utilisateur. 
Le transcient est un hostname dynamique maintenu par le kernel

### 4. Gestion du réseau (et résolution de noms)

- * afficher les informations DHCP récupérées par NetworkManager (sur une interface en DHCP)*

On lance la commande `nmcli con show enp1s0`. On récupère les informations DHCP à la fin de l'output.

```
DHCP4.OPTION[1]:                        broadcast_address = 192.168.122.255
DHCP4.OPTION[2]:                        dhcp_lease_time = 3600
DHCP4.OPTION[3]:                        dhcp_rebinding_time = 3035
DHCP4.OPTION[4]:                        dhcp_renewal_time = 1685
DHCP4.OPTION[5]:                        dhcp_server_identifier = 192.168.122.1
DHCP4.OPTION[6]:                        domain_name_servers = 192.168.122.1
DHCP4.OPTION[7]:                        expiry = 1575540449
DHCP4.OPTION[8]:                        ip_address = 192.168.122.145
DHCP4.OPTION[9]:                        next_server = 192.168.122.1
DHCP4.OPTION[10]:                       requested_broadcast_address = 1
DHCP4.OPTION[11]:                       requested_dhcp_server_identifier = 1
DHCP4.OPTION[12]:                       requested_domain_name = 1
DHCP4.OPTION[13]:                       requested_domain_name_servers = 1
DHCP4.OPTION[14]:                       requested_domain_search = 1
DHCP4.OPTION[15]:                       requested_host_name = 1
DHCP4.OPTION[16]:                       requested_interface_mtu = 1
DHCP4.OPTION[17]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[18]:                       requested_nis_domain = 1
DHCP4.OPTION[19]:                       requested_nis_servers = 1
DHCP4.OPTION[20]:                       requested_ntp_servers = 1
DHCP4.OPTION[21]:                       requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[22]:                       requested_root_path = 1
DHCP4.OPTION[23]:                       requested_routers = 1
DHCP4.OPTION[24]:                       requested_static_routes = 1
DHCP4.OPTION[25]:                       requested_subnet_mask = 1
DHCP4.OPTION[26]:                       requested_time_offset = 1
DHCP4.OPTION[27]:                       requested_wpad = 1
DHCP4.OPTION[28]:                       routers = 192.168.122.1
DHCP4.OPTION[29]:                       subnet_mask = 255.255.255.0
```

- *stopper et désactiver le démarrage de NetworkManager*

`sudo systemctl stop NetworkManager` \
`sudo systemctl disable NetworkManager` 

- *démarrer et activer le démarrage de systemd-networkd*

`sudo systemctl start systemd-networkd`\
`sudo systemctl enable systemd-networkd`

- *éditer la configuration d'une carte réseau de la VM avec un fichier .network*


`sudo nano /etc/systemd/network/interface.network`

```
[Match]
Key=enp1s0

[Network]
Address=192.168.122.145/24
DNS=1.1.1.1
```

- *activer la résolution de noms par systemd-resolved en démarrant le service (maintenant et au boot)*

`sudo systemctl start systemd-resolved`\
`sudo systemctl enable systemd-resolved`\

- *vérifier que le service est lancé*

`sudo systemctl status systemd-resolved`

### 6. Gestion d'unité basique (services)

- *trouver l'unité associée au processus chronyd*

```
[enigmind@localhost ~]$ systemctl list-unit-files | grep chrony
chrony-dnssrv@.service                     static         
chrony-wait.service                        disabled       
chronyd.service                            disabled       
chrony-dnssrv@.timer                       disabled 
```

## II. Boot et Logs

- *déterminer le temps qu'a mis sshd.service à démarrer*

`systemd-analyze blame | grep sshd`\
`50ms sshd.service `

## III. Mécanismes manipulés par systemd

### 1. cgroups
- *identifier le cgroup utilisé par votre session SSH*

```
[enigmind@localhost ~]$ sudo systemd-cgls
[sudo] Mot de passe de enigmind : 
Control group /:
-.slice
├─user.slice
│ ├─user-0.slice
│ │ ├─session-1.scope
│ │ │ ├─854 login -- root
│ │ │ └─967 -bash
│ │ └─user@0.service
│ │   └─init.scope
│ │     ├─960 /usr/lib/systemd/systemd --user
│ │     └─962 (sd-pam)
│ └─user-1000.slice
│   ├─user@1000.service
│   │ └─init.scope
│   │   ├─995 /usr/lib/systemd/systemd --user
│   │   └─997 (sd-pam)
│   └─session-5.scope
│     ├─1710 sshd: enigmind [priv]
│     ├─1714 sshd: enigmind@pts/1
│     ├─1715 -bash
│     ├─2307 sudo systemd-cgls
│     ├─2313 systemd-cgls
│     └─2314 less

```
- *identifier la RAM maximale à votre disposition (dans /sys/fs/cgroup)*

```
[enigmind@localhost ~]$ cat /sys/fs/cgroup/memory/memory.max_usage_in_bytes 
741134336
```

- *modifier la RAM dédiée à votre session utilisateur*



*la commande `systemctl set-property` génère des fichiers dans /etc/systemd/system.control/*
- *vérifier la création du fichier*

## 2. D-Bus
